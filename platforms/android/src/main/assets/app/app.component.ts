import { Component, OnInit } from "@angular/core";
import { SnackBar, SnackBarOptions } from "nativescript-snackbar";

@Component({
  selector: "my-app",
  templateUrl: "./app.component.html",
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  // Your TypeScript logic goes here
  message: string;
  action: string;
  snackbar: SnackBar;
  response: string;
  path: string;
  metadata: string;
  //mmr: MediaMetadataRetriever;

  constructor() {
    this.message = "Hello :)";
    this.action = "CLEAR";
    this.response = "response";
    this.path = "/sdcard/UCDowloads/Hey Baby (feat. Deb's Daughter).mp3";
    this.metadata = "metadata";
  }

  ngOnInit(): void {
    this.snackbar = new SnackBar();
    //this.mmr = new MediaMetadataRetriever();
    this.showSimpleSnackBar();
  }

  showSimpleSnackBar() {
    this.snackbar.simple(this.message)
      .then((args) => {
        this.response = JSON.stringify(args);
       });
  }

  showActionSnackBar() {
    let options: SnackBarOptions = {
      actionText: this.action,
      actionTextColor: "#48f442",
      snackText: this.message,
      hideDelay: 3000
    }
    this.snackbar.action(options).then((args) => {
      //Do something after getting response
      if(args.command === "Action") {
        this.response = JSON.stringify(args);
        if (this.action === "CLEAR") {
          this.message = '';
          this.action = '';
        } else if (this.action === "ADD") {
          this.message = 'Message Added';
          this.action = 'CLEAR';
        }
      } else {
        this.response = JSON.stringify(args);
      }
    });
  }

  getMetadata() {
    //this.mmr.setDataSource(this.path);
    //this.metadata = this.mmr.extractMetadata(MediaMetadataRetriever._METADATA_KEY_TITLE);
  }
}
