"use strict";
var core_1 = require("@angular/core");
var nativescript_snackbar_1 = require("nativescript-snackbar");
var AppComponent = (function () {
    //mmr: MediaMetadataRetriever;
    function AppComponent() {
        this.message = "Hello :)";
        this.action = "CLEAR";
        this.response = "response";
        this.path = "/sdcard/UCDowloads/Hey Baby (feat. Deb's Daughter).mp3";
        this.metadata = "metadata";
    }
    AppComponent.prototype.ngOnInit = function () {
        this.snackbar = new nativescript_snackbar_1.SnackBar();
        //this.mmr = new MediaMetadataRetriever();
        this.showSimpleSnackBar();
    };
    AppComponent.prototype.showSimpleSnackBar = function () {
        var _this = this;
        this.snackbar.simple(this.message)
            .then(function (args) {
            _this.response = JSON.stringify(args);
        });
    };
    AppComponent.prototype.showActionSnackBar = function () {
        var _this = this;
        var options = {
            actionText: this.action,
            actionTextColor: "#48f442",
            snackText: this.message,
            hideDelay: 3000
        };
        this.snackbar.action(options).then(function (args) {
            //Do something after getting response
            if (args.command === "Action") {
                _this.response = JSON.stringify(args);
                if (_this.action === "CLEAR") {
                    _this.message = '';
                    _this.action = '';
                }
                else if (_this.action === "ADD") {
                    _this.message = 'Message Added';
                    _this.action = 'CLEAR';
                }
            }
            else {
                _this.response = JSON.stringify(args);
            }
        });
    };
    AppComponent.prototype.getMetadata = function () {
        //this.mmr.setDataSource(this.path);
        //this.metadata = this.mmr.extractMetadata(MediaMetadataRetriever._METADATA_KEY_TITLE);
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: "my-app",
        templateUrl: "./app.component.html",
        styleUrls: ['./app.component.css']
    }),
    __metadata("design:paramtypes", [])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHNDQUFrRDtBQUNsRCwrREFBa0U7QUFRbEUsSUFBYSxZQUFZO0lBUXZCLDhCQUE4QjtJQUU5QjtRQUNFLElBQUksQ0FBQyxPQUFPLEdBQUcsVUFBVSxDQUFDO1FBQzFCLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxRQUFRLEdBQUcsVUFBVSxDQUFDO1FBQzNCLElBQUksQ0FBQyxJQUFJLEdBQUcsd0RBQXdELENBQUM7UUFDckUsSUFBSSxDQUFDLFFBQVEsR0FBRyxVQUFVLENBQUM7SUFDN0IsQ0FBQztJQUVELCtCQUFRLEdBQVI7UUFDRSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksZ0NBQVEsRUFBRSxDQUFDO1FBQy9CLDBDQUEwQztRQUMxQyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBRUQseUNBQWtCLEdBQWxCO1FBQUEsaUJBS0M7UUFKQyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO2FBQy9CLElBQUksQ0FBQyxVQUFDLElBQUk7WUFDVCxLQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEMsQ0FBQyxDQUFDLENBQUM7SUFDUixDQUFDO0lBRUQseUNBQWtCLEdBQWxCO1FBQUEsaUJBc0JDO1FBckJDLElBQUksT0FBTyxHQUFvQjtZQUM3QixVQUFVLEVBQUUsSUFBSSxDQUFDLE1BQU07WUFDdkIsZUFBZSxFQUFFLFNBQVM7WUFDMUIsU0FBUyxFQUFFLElBQUksQ0FBQyxPQUFPO1lBQ3ZCLFNBQVMsRUFBRSxJQUFJO1NBQ2hCLENBQUE7UUFDRCxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBQyxJQUFJO1lBQ3RDLHFDQUFxQztZQUNyQyxFQUFFLENBQUEsQ0FBQyxJQUFJLENBQUMsT0FBTyxLQUFLLFFBQVEsQ0FBQyxDQUFDLENBQUM7Z0JBQzdCLEtBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDckMsRUFBRSxDQUFDLENBQUMsS0FBSSxDQUFDLE1BQU0sS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDO29CQUM1QixLQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztvQkFDbEIsS0FBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUM7Z0JBQ25CLENBQUM7Z0JBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLEtBQUksQ0FBQyxNQUFNLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQztvQkFDakMsS0FBSSxDQUFDLE9BQU8sR0FBRyxlQUFlLENBQUM7b0JBQy9CLEtBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDO2dCQUN4QixDQUFDO1lBQ0gsQ0FBQztZQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNOLEtBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN2QyxDQUFDO1FBQ0gsQ0FBQyxDQUFDLENBQUM7SUFDTCxDQUFDO0lBRUQsa0NBQVcsR0FBWDtRQUNFLG9DQUFvQztRQUNwQyx1RkFBdUY7SUFDekYsQ0FBQztJQUNILG1CQUFDO0FBQUQsQ0FBQyxBQTNERCxJQTJEQztBQTNEWSxZQUFZO0lBTnhCLGdCQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsUUFBUTtRQUNsQixXQUFXLEVBQUUsc0JBQXNCO1FBQ25DLFNBQVMsRUFBRSxDQUFDLHFCQUFxQixDQUFDO0tBQ25DLENBQUM7O0dBRVcsWUFBWSxDQTJEeEI7QUEzRFksb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBTbmFja0JhciwgU25hY2tCYXJPcHRpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1zbmFja2JhclwiO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwibXktYXBwXCIsXG4gIHRlbXBsYXRlVXJsOiBcIi4vYXBwLmNvbXBvbmVudC5odG1sXCIsXG4gIHN0eWxlVXJsczogWycuL2FwcC5jb21wb25lbnQuY3NzJ11cbn0pXG5cbmV4cG9ydCBjbGFzcyBBcHBDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAvLyBZb3VyIFR5cGVTY3JpcHQgbG9naWMgZ29lcyBoZXJlXG4gIG1lc3NhZ2U6IHN0cmluZztcbiAgYWN0aW9uOiBzdHJpbmc7XG4gIHNuYWNrYmFyOiBTbmFja0JhcjtcbiAgcmVzcG9uc2U6IHN0cmluZztcbiAgcGF0aDogc3RyaW5nO1xuICBtZXRhZGF0YTogc3RyaW5nO1xuICAvL21tcjogTWVkaWFNZXRhZGF0YVJldHJpZXZlcjtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICB0aGlzLm1lc3NhZ2UgPSBcIkhlbGxvIDopXCI7XG4gICAgdGhpcy5hY3Rpb24gPSBcIkNMRUFSXCI7XG4gICAgdGhpcy5yZXNwb25zZSA9IFwicmVzcG9uc2VcIjtcbiAgICB0aGlzLnBhdGggPSBcIi9zZGNhcmQvVUNEb3dsb2Fkcy9IZXkgQmFieSAoZmVhdC4gRGViJ3MgRGF1Z2h0ZXIpLm1wM1wiO1xuICAgIHRoaXMubWV0YWRhdGEgPSBcIm1ldGFkYXRhXCI7XG4gIH1cblxuICBuZ09uSW5pdCgpOiB2b2lkIHtcbiAgICB0aGlzLnNuYWNrYmFyID0gbmV3IFNuYWNrQmFyKCk7XG4gICAgLy90aGlzLm1tciA9IG5ldyBNZWRpYU1ldGFkYXRhUmV0cmlldmVyKCk7XG4gICAgdGhpcy5zaG93U2ltcGxlU25hY2tCYXIoKTtcbiAgfVxuXG4gIHNob3dTaW1wbGVTbmFja0JhcigpIHtcbiAgICB0aGlzLnNuYWNrYmFyLnNpbXBsZSh0aGlzLm1lc3NhZ2UpXG4gICAgICAudGhlbigoYXJncykgPT4ge1xuICAgICAgICB0aGlzLnJlc3BvbnNlID0gSlNPTi5zdHJpbmdpZnkoYXJncyk7XG4gICAgICAgfSk7XG4gIH1cblxuICBzaG93QWN0aW9uU25hY2tCYXIoKSB7XG4gICAgbGV0IG9wdGlvbnM6IFNuYWNrQmFyT3B0aW9ucyA9IHtcbiAgICAgIGFjdGlvblRleHQ6IHRoaXMuYWN0aW9uLFxuICAgICAgYWN0aW9uVGV4dENvbG9yOiBcIiM0OGY0NDJcIixcbiAgICAgIHNuYWNrVGV4dDogdGhpcy5tZXNzYWdlLFxuICAgICAgaGlkZURlbGF5OiAzMDAwXG4gICAgfVxuICAgIHRoaXMuc25hY2tiYXIuYWN0aW9uKG9wdGlvbnMpLnRoZW4oKGFyZ3MpID0+IHtcbiAgICAgIC8vRG8gc29tZXRoaW5nIGFmdGVyIGdldHRpbmcgcmVzcG9uc2VcbiAgICAgIGlmKGFyZ3MuY29tbWFuZCA9PT0gXCJBY3Rpb25cIikge1xuICAgICAgICB0aGlzLnJlc3BvbnNlID0gSlNPTi5zdHJpbmdpZnkoYXJncyk7XG4gICAgICAgIGlmICh0aGlzLmFjdGlvbiA9PT0gXCJDTEVBUlwiKSB7XG4gICAgICAgICAgdGhpcy5tZXNzYWdlID0gJyc7XG4gICAgICAgICAgdGhpcy5hY3Rpb24gPSAnJztcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLmFjdGlvbiA9PT0gXCJBRERcIikge1xuICAgICAgICAgIHRoaXMubWVzc2FnZSA9ICdNZXNzYWdlIEFkZGVkJztcbiAgICAgICAgICB0aGlzLmFjdGlvbiA9ICdDTEVBUic7XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoaXMucmVzcG9uc2UgPSBKU09OLnN0cmluZ2lmeShhcmdzKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfVxuXG4gIGdldE1ldGFkYXRhKCkge1xuICAgIC8vdGhpcy5tbXIuc2V0RGF0YVNvdXJjZSh0aGlzLnBhdGgpO1xuICAgIC8vdGhpcy5tZXRhZGF0YSA9IHRoaXMubW1yLmV4dHJhY3RNZXRhZGF0YShNZWRpYU1ldGFkYXRhUmV0cmlldmVyLl9NRVRBREFUQV9LRVlfVElUTEUpO1xuICB9XG59XG4iXX0=