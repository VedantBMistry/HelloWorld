"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var media_metadata_retriever_common_1 = require("./media-metadata-retriever.common");
var MediaMetadataRetriever = (function (_super) {
    __extends(MediaMetadataRetriever, _super);
    function MediaMetadataRetriever() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MediaMetadataRetriever.prototype.setDataSource = function (path) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            try {
                if (path) {
                    _this._mediaMetadataRetriever = new android.media.MediaMetadataRetriever();
                    _this._mediaMetadataRetriever.setDataSource(path);
                }
                else {
                    reject('path is required');
                }
            }
            catch (ex) {
                reject(ex);
            }
        });
    };
    MediaMetadataRetriever.prototype.extractMetadata = function (keyCode) {
        try {
            if (keyCode) {
                this._mediaMetadataRetriever = new android.media.MediaMetadataRetriever();
                return this._mediaMetadataRetriever.extractMetadata(keyCode);
            }
        }
        catch (ex) {
            return null;
        }
        return null;
    };
    MediaMetadataRetriever.prototype.getEmbeddedPicture = function () {
        try {
            this._mediaMetadataRetriever = new android.media.MediaMetadataRetriever();
            return this._mediaMetadataRetriever.getEmbeddedPicture();
        }
        catch (ex) {
            return null;
        }
    };
    return MediaMetadataRetriever;
}(media_metadata_retriever_common_1.Common));
MediaMetadataRetriever._METADATA_KEY_ALBUM = android.media.MediaMetadataRetriever.METADATA_KEY_ALBUM;
MediaMetadataRetriever._METADATA_KEY_ALBUMARTIST = android.media.MediaMetadataRetriever.METADATA_KEY_ALBUMARTIST;
MediaMetadataRetriever._METADATA_KEY_ARTIST = android.media.MediaMetadataRetriever.METADATA_KEY_ARTIST;
MediaMetadataRetriever._METADATA_KEY_AUTHOR = android.media.MediaMetadataRetriever.METADATA_KEY_AUTHOR;
MediaMetadataRetriever._METADATA_KEY_BITRATE = android.media.MediaMetadataRetriever.METADATA_KEY_BITRATE;
MediaMetadataRetriever._METADATA_KEY_CD_TRACK_NUMBER = android.media.MediaMetadataRetriever.METADATA_KEY_CD_TRACK_NUMBER;
MediaMetadataRetriever._METADATA_KEY_COMPILATION = android.media.MediaMetadataRetriever.METADATA_KEY_COMPILATION;
MediaMetadataRetriever._METADATA_KEY_COMPOSER = android.media.MediaMetadataRetriever.METADATA_KEY_COMPOSER;
MediaMetadataRetriever._METADATA_KEY_DATE = android.media.MediaMetadataRetriever.METADATA_KEY_DATE;
MediaMetadataRetriever._METADATA_KEY_DISK_NUMBER = android.media.MediaMetadataRetriever.METADATA_KEY_DISC_NUMBER;
MediaMetadataRetriever._METADATA_KEY_DURATION = android.media.MediaMetadataRetriever.METADATA_KEY_DURATION;
MediaMetadataRetriever._METADATA_KEY_GENRE = android.media.MediaMetadataRetriever.METADATA_KEY_GENRE;
MediaMetadataRetriever._METADATA_KEY_HAS_AUDIO = android.media.MediaMetadataRetriever.METADATA_KEY_HAS_AUDIO;
MediaMetadataRetriever._METADATA_KEY_HAS_VIDEO = android.media.MediaMetadataRetriever.METADATA_KEY_HAS_VIDEO;
MediaMetadataRetriever._METADATA_KEY_HAS_LOCATION = android.media.MediaMetadataRetriever.METADATA_KEY_LOCATION;
MediaMetadataRetriever._METADATA_KEY_HAS_MIMETYPE = android.media.MediaMetadataRetriever.METADATA_KEY_MIMETYPE;
MediaMetadataRetriever._METADATA_KEY_NUM_TRACKS = android.media.MediaMetadataRetriever.METADATA_KEY_NUM_TRACKS;
MediaMetadataRetriever._METADATA_KEY_TITLE = android.media.MediaMetadataRetriever.METADATA_KEY_TITLE;
MediaMetadataRetriever._METADATA_KEY_VIDEO_HEIGHT = android.media.MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT;
MediaMetadataRetriever._METADATA_KEY_VIDEO_ROTATION = android.media.MediaMetadataRetriever.METADATA_KEY_VIDEO_ROTATION;
MediaMetadataRetriever._METADATA_KEY_VIDEO_WIDTH = android.media.MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH;
MediaMetadataRetriever._METADATA_KEY_WRITE = android.media.MediaMetadataRetriever.METADATA_KEY_WRITER;
MediaMetadataRetriever._METADATA_KEY_YEAR = android.media.MediaMetadataRetriever.METADATA_KEY_YEAR;
exports.MediaMetadataRetriever = MediaMetadataRetriever;
//# sourceMappingURL=media-metadata-retriever.js.map